import { Component, ViewChild } from '@angular/core';
import { Platform, App, Nav, } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import{Deeplinks} from '@ionic-native/deeplinks';
import { Utils } from '../Controllers/Utils/utils';
import { PostServiceProvider } from '../providers/post-service/post-service';
import { StorageClass } from '../Controllers/Storage/Storage';
import { global } from '../Controllers/test';
import { TabsPage } from '../pages/PagesTabs/tabs/tabs';
import { HttpErrorResponse } from '@angular/common/http';
import { LoginPage } from '../pages/login/login';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage:any = 'LoginPage';

  constructor(public app:App,private deeplink:Deeplinks,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    public utils:Utils,public postService:PostServiceProvider,public StorageClass:StorageClass) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      var test;
      this.deeplink.route({
        '/':{},
        '/confirmaccount': "ConfirmaccountPage",
        '/changepassword': "ChangePassword",
        '/web':{"web":true}
      }).subscribe((match)=>{
        
        JSON.stringify(match, function (key, value) {
          var obj = JSON.stringify(match.$args);
          var serialize = JSON.parse(obj);
          if (key == "$route" && value == "ConfirmaccountPage") {

            app.getRootNav().push("ConfirmaccountPage",{Token:serialize.token,isChangepassword:false});
              }else if (key == "$route" && value == "ChangePassword"){
                app.getRootNav().push("ConfirmaccountPage",{Token:serialize.token,isChangepassword:true});
              }else{
                return value
              }
              
        })
      },(noMacth)=>{
        
      });
     /* StorageClass.get("auto").then((data:any) =>{
        alert(data)
        console.log(data)
        this.AutoLogin(data.email,data.password);
      (err) => {
        console.log(err)
      }
      }).catch(err=> console.log(err));*/
     
      if(platform.is('android')) {
        statusBar.overlaysWebView(true);
        statusBar.hide();
        statusBar.backgroundColorByHexString("#33000000");
      }else{
       
        statusBar.hide();
        
        statusBar.overlaysWebView(true);
       
      }
      
      splashScreen.hide();
    });
  }
  
  AutoLogin(email,password){
    this.utils.Progress("começando sessão");

    let body ={
      "email": email,
      "password": password
  };
  
    
this.postService.login(body).then((data:any)=>{
  var response = JSON.parse(data)
  console.log(response)
  if(response.success){
    this.StorageClass.set("Token",response.data.token)
    global.length = 0;
    global.push(response);
    this.app.getRootNav().setRoot(TabsPage);
   
  }else{
    alert("E-mail ou senha incorretos")
    this.utils.HidenLoad();
    this.app.getRootNav().setRoot("LoginPage");
  }
  
},(err: HttpErrorResponse) => {
  this.utils.HidenLoad();
  if(err.status==401){
    alert("E-mail ou senha incorretos")
  }
  if (err.error instanceof Error) {
    console.log('Client-side error occured.');
  } else {
    if(err.status == 3){
      this.utils.PopUp("Não há conexão com a internet","Tente novamente");
      
    }
  }
  this.app.getRootNav().setRoot("LoginPage");
}).catch((ex:any)=>{
  this.utils.HidenLoad();
  console.log(ex.error)
  if(ex.status == 3){
    this.utils.PopUp("Não há conexão com a internet","Tente novamente");
  }
  this.app.getRootNav().setRoot("LoginPage");
})
 /* global.length = 0;
    global.push(this.test);
this.navCtrl.setRoot(TabsPage);*/
  }
}
