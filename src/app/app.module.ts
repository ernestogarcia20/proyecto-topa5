import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { PastbetsPage } from '../pages/PagesTabs/pastbets/pastbets';
import { MyBetPage } from '../pages/PagesTabs/mybet/mybet';
import { HomePage } from '../pages/PagesTabs/home/home';
import { TabsPage } from '../pages/PagesTabs/tabs/tabs';
//import { RankingPage } from '../pages/PagesTabs/ranking/ranking';
import { HTTP } from '@ionic-native/http';
import{Deeplinks} from '@ionic-native/deeplinks';
import { HttpClientModule} from '@angular/common/http';
import {Utils}from '../Controllers/Utils/utils';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Keyboard} from '@ionic-native/keyboard'
import { PopoverComponent } from '../components/popover/popover';
import { CatalogServiceProvider } from '../providers/catalog-service/catalog-service';
import { PostServiceProvider } from '../providers/post-service/post-service';
import { IonicStorageModule } from '@ionic/storage';
import {StorageClass } from '../Controllers/Storage/Storage';
import {sesion } from '../Controllers/sesion';
import {SecureStorage } from '@ionic-native/secure-storage';

@NgModule({
  declarations: [
    MyApp,
    PastbetsPage,
    MyBetPage,
    HomePage,
    TabsPage,
    PopoverComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    
    IonicModule.forRoot(MyApp,{preloadModules: true,scrollAssist: false,tabsHideOnSubPages: false,autoFocusAssist: false})
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PastbetsPage,
    MyBetPage,
    HomePage,
    TabsPage,
    PopoverComponent,
  ],
  providers: [
    StatusBar,
    Utils,
    sesion,
    StorageClass,
    SecureStorage,
    SplashScreen,
    Keyboard,
    HTTP,
    Deeplinks,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CatalogServiceProvider,
    PostServiceProvider
  ]
})
export class AppModule {}
