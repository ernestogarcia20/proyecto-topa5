import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the PopoverComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'popover',
  templateUrl: 'popover.html'
})
export class PopoverComponent {

  text: string;
  betnum: Array<{title: string,betnum1:number,betnum2:number}>;
  constructor(public viewCtrl: ViewController,public navParams:NavParams) {
    this.loadPopover(this.navParams.get('key'))
  }
  loadPopover(key){
    this.betnum =[];
    if(key=="win1"){
      for(var i = 1; i<=9;i++){
        for(var j = 0;j<i;j++){
          this.betnum.push(
            {title:i+"x"+j,betnum1:i,betnum2:j});
        }
      }
    }
    else if(key=="win2"){
      for(var i = 1; i<=9;i++){
        for(var j = 0;j<i;j++){
          this.betnum.push(
            {title:j+"x"+i,betnum1:j,betnum2:i});
        }
      }
    }else{
      for(var i = 0; i<=9;i++){
        this.betnum.push(
          {title:i+"x"+i,betnum1:i,betnum2:i});
      }
    }


  }
  close(select) {
    this.viewCtrl.dismiss(select);
  }
}
