import { HTTP } from '@ionic-native/http';
import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Utils } from '../../Controllers/Utils/utils';
import { App } from 'ionic-angular';

/*
  Generated class for the CatalogServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CatalogServiceProvider {
  apiUrl = "https://api-topa5.remingtonexchange.io/api";
  headers:any;
  local:Storage;
  constructor(public https:HTTP,public utils:Utils,public app: App) {
    
  }

  verifyAccount(token){
    return new Promise((resolve, reject) => {
      this.https.setDataSerializer('json');
        this.https.get(this.apiUrl+'/users/verify_token/'+token, null,{"Content-Type": "application/json"}).then((res) => {
          resolve(JSON.parse(res.data));
        },(err: HttpErrorResponse) => {
          this.HttpsError(err);
           reject(err);
          });
  });
  }
  ValidPasswordToken(token){
    return new Promise((resolve, reject) => {
      this.https.setDataSerializer('json');
        this.https.get(this.apiUrl+'/users/reset_password_token/'+token, null,{"Content-Type": "application/json"}).then((res) => {
          resolve(JSON.parse(res.data));
        },(err: HttpErrorResponse) => {
          this.HttpsError(err);
           reject(err);
          });
  });
  }
  HttpsError(err: HttpErrorResponse){
    if (err.error instanceof Error) {
      console.log('Client-side error occured.');
    } else {
      if(err.status == 3){
        this.utils.PopUp("Não há conexão com a internet","Tente novamente");
      }else if (err.status == 404){
        this.utils.PopUp("Sua sessão expirou","");
        this.app.getRootNav().setRoot(("LoginPage"));
      }else if (err.status == 401){
        this.utils.PopUp("Sua sessão expirou","");
        this.app.getRootNav().setRoot(("LoginPage"));
      }else if(err.status == 408){
        this.utils.PopUp("Não é possível estabelecer uma conexão","Tente novamente");
      }
    
    }
    this.utils.HidenLoad();
  }
  

}
