
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { HTTP } from '@ionic-native/http';
import { global} from '../../Controllers/test';
import { HttpErrorResponse } from '@angular/common/http';
import { Utils } from '../../Controllers/Utils/utils';
import { App } from 'ionic-angular';
/*
  Generated class for the PostServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PostServiceProvider {
headers:any;
local:Storage;
apiurl:string = "https://api-topa5.remingtonexchange.io/api";
//apiurl:string = "/api"
  constructor(public app:App,public https:HTTP,public utils:Utils) {
  }

  login(data){
    return new Promise((resolve, reject) => {
      this.https.setDataSerializer('json');
        this.https.post(this.apiurl+'/users/token', data, {"Content-Type": "application/json"}).then((res) => {
          resolve(res.data);
        }, (err) => {
          reject(err);
        });
  });
  }
  registry(data){
          return new Promise((resolve, reject) => {
            this.https.setDataSerializer('json');
            this.https.post(this.apiurl+'/users/register', data,  {"Content-Type": "application/json"}).then((res) => {
              resolve(res.data);
            }, (err) => {
              reject(err);
            });
        });
   
  }
  LogOut(data){
    return new Promise((resolve, reject) => {
      this.https.setDataSerializer('json');
      this.https.post(this.apiurl+'/users/logout', data, {"Content-Type": "application/json"}).then((res) => {
        console.log(res.data)
        resolve(res.data);
      },(err) => {
          
        reject(err);
      });
  });
  }

  ForgotPassword(data){
    return new Promise((resolve, reject) => {
      this.https.setDataSerializer('json');
      this.https.post(this.apiurl+'/users/forgot_password', data, {"Content-Type": "application/json"}).then((res) => {
        
        resolve(res.data);
      }, (err) => {
        reject(err);
      });
  });
  }
  ChangePassword(data){
    return new Promise((resolve, reject) => {
      this.https.setDataSerializer('json');
      this.https.post(this.apiurl+'/users/changepassword', data, {"Content-Type": "application/json"}).then((res) => {
        
        resolve(res.data);
      }, (err) => {
        reject(err);
      });
  });
  }


  ListBetGames(data){
    return new Promise((resolve, reject) => {
      this.https.setHeader("","","")
      this.https.setDataSerializer('json');
      this.https.post(this.apiurl+'/apuestas/apuestasByIduser', data, {"Content-Type": "application/json",'Authorization':'Bearer ' + global[0].data.token }).then((res) => {
    
        resolve(JSON.parse(res.data));
      },(err: HttpErrorResponse) => {
        
        this.HttpsError(err);
        reject(err);
      });

    });

  }

  ListPastGames(data){
    return new Promise((resolve, reject) => {
      this.https.setHeader("","","")
      this.https.setDataSerializer('json');
      this.https.post(this.apiurl+'/leagues/lastgames', data, {"Content-Type": "application/json",'Authorization':'Bearer ' + global[0].data.token }).then((res) => {
    
        resolve(JSON.parse(res.data));
      },(err: HttpErrorResponse) => {
        
        this.HttpsError(err);
        reject(err);
      });

    });

  }

  Balance(data){
    return new Promise((resolve, reject) => {
      this.https.setHeader("","","")
      this.https.setDataSerializer('json');
      this.https.post(this.apiurl+'/balances/consolidadoByIduser', data, {"Content-Type": "application/json",'Authorization':'Bearer ' + global[0].data.token }).then((res) => {
    
        resolve(JSON.parse(res.data));
      },(err: HttpErrorResponse) => {
        
        this.HttpsError(err);
        reject(err);
      }).catch(ex=>reject(ex));

    });
  }

  BetGame(data){
    return new Promise((resolve, reject) => {
      this.https.setHeader("","","")
      this.https.setDataSerializer('json');
      this.https.post(this.apiurl+'/apuestas/add', data, {"Content-Type": "application/json",'Authorization':'Bearer ' + global[0].data.token }).then((res) => {
        
        resolve(JSON.parse(res.data));
      },(err: HttpErrorResponse) => {
        this.HttpsError(err);
         reject(err);
        });
  });
  }

  Games(data){
    return new Promise((resolve, reject) => {
      this.https.setHeader("","","")
      this.https.setDataSerializer('json');
      this.https.post(this.apiurl+'/leagues/nextgames', data, {"Content-Type": "application/json",'Authorization':'Bearer ' + global[0].data.token }).then((res) => {
        
        resolve(JSON.parse(res.data));
      },(err: HttpErrorResponse) => {
        //this.HttpsError(err);
         reject(err);
        });
  });
  }
 
  HttpsError(err: HttpErrorResponse){
    if (err.error instanceof Error) {
      console.log('Client-side error occured.');
    } else {
      if(err.status == 3){
        this.utils.PopUp("Não há conexão com a internet","Tente novamente");
      }else if (err.status == 404){
        this.utils.PopUp("Sua sessão expirou","");
        this.app.getRootNav().setRoot(("LoginPage"));
      }else if (err.status == 401){
        this.utils.PopUp("Sua sessão expirou","");
        this.app.getRootNav().setRoot(("LoginPage"));
      }else if(err.status == 408){
        this.utils.PopUp("Não é possível estabelecer uma conexão","Tente novamente");
      }
    
    }
    this.utils.HidenLoad();
  }

}
