import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaguesPage } from './leagues';

@NgModule({
  declarations: [
    LeaguesPage,
  ],
  imports: [
    IonicPageModule.forChild(LeaguesPage),
  ],
})
export class LeaguesPageModule {}
