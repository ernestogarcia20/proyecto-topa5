import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the LeaguesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-leagues',
  templateUrl: 'leagues.html',
})
export class LeaguesPage {
  players: Array<{title: string,  background:string,id:number}>;
  isGamen:boolean;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.isGamen = navParams.get('isGamen');
  }

  ionViewWillEnter() {
    this.players =[];
    this.players.push(
      {title:"Brasileiro",background:"brasila.jpg",id:2},
      {title:"Copa do Brasil",background:"copadobrasl.png",id:2},
      {title:"Libertadores",background:"libertadores.jpg",id:0},
      {title:"Champions",background:"champions.png",id:1},
    )
  }
  Games(_player){
    
    if(this.isGamen){
      this.navCtrl.push("GamesPage",{Id:_player.id});
    }
    else {
      this.navCtrl.push("ClassificationPage",{Title:_player.title});
    }
    
  }


}

