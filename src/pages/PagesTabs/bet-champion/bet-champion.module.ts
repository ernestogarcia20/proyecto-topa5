import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BetChampionPage } from './bet-champion';

@NgModule({
  declarations: [
    BetChampionPage,
  ],
  imports: [
    IonicPageModule.forChild(BetChampionPage),
  ],
})
export class BetChampionPageModule {}
