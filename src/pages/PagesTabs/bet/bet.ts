import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, DateTime } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { PopoverComponent } from '../../../components/popover/popover';
import { balance,global } from '../../../Controllers/test';
import { Utils } from '../../../Controllers/Utils/utils';
import { PostServiceProvider } from '../../../providers/post-service/post-service';
/**
 * Generated class for the BetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bet',
  templateUrl: 'bet.html',
})
export class BetPage {
  @ViewChild('slider') slider: Slides;
  @ViewChild("segments") segments;
  page: any;
  open:boolean = false;
  resultwin:any=0;
  resultlost:any=0;
  betresult1:string = "-";
  betresult2:string = "-";
  bettie:string= "0x0";
  DetailGame:any;
  Date:any;
  Time:any;
  Balance:Balance;
  wallet;
  User:user;
  idLiga:any;
  constructor(public popoverCtrl: PopoverController,
    public utils:Utils,public postService:PostServiceProvider,
    public navCtrl: NavController, public navParams: NavParams) {
    console.log(navParams.get('Games'))
    this.DetailGame = navParams.get('Games');
    this.idLiga = navParams.get('Liga');
    //this.Date = new Date(this.DetailGame.utcDate).toISOString();

  }
  presentPopover(myEvent,name) {
   
    let popover = this.popoverCtrl.create(PopoverComponent,{key:name});
    popover.present({
      ev: myEvent
    });
    popover.onDidDismiss((reponse)=>{
      if(reponse!=null){
     if(name=="win1"){
       this.resultwin = reponse.betnum1;
       this.resultlost = reponse.betnum2;
       this.betresult1 = reponse.title;
       this.betresult2 = "-";
       this.bettie = "-";
     }else if(name=="tie"){
      this.resultwin = reponse.betnum1;
      this.resultlost = reponse.betnum2;
      this.betresult1 = "-";
      this.betresult2 = "-";
      this.bettie = reponse.title;

     }else{
      this.resultwin = reponse.betnum1;
      this.resultlost = reponse.betnum2;
      this.betresult1 = "-";
      this.betresult2 =reponse.title;
      this.bettie = "-"; 

     }
    }

    });
  }
  ionViewWillEnter() {
    this.Balance = balance[0];
    this.wallet = parseFloat(this.Balance.wallet).toFixed(2);
    this.slideChanged();
  }
  Confirme(){
    this.utils.Progress2("Carregando");
    this.User = global[0].user;
    var idganador;
    var idperdedor;
    var golesganador;
    var golesperdedor;

    if(this.resultwin >= this.resultlost){
      golesganador = this.resultwin;
      golesperdedor = this.resultlost;
      idganador = this.DetailGame.homeTeam_id;
      idperdedor = this.DetailGame.awayTeam_id;
    }else if(this.resultlost >= this.resultwin){
      golesganador = this.resultlost;
      golesperdedor = this.resultwin;
      idganador = this.DetailGame.awayTeam_id;
      idperdedor = this.DetailGame.homeTeam_id;
    }
    let post = {
      "user_id":this.User.id,
      "juego_id":this.DetailGame.fixture_id,
      "liga_id":this.idLiga,
      "temporada_id":2,
      "amount":5,
      "equipo_id_ganador":idganador,
      "goles_ganador": golesganador,
      "tipo": 1,  
      "equipo_id_perdedor": idperdedor,
      "goles_perdedor":golesperdedor

    };
    this.postService.BetGame(post).then((data:any)=>{
      if(data!=null){
        this.utils.HidenLoad();
        this.utils.PopUp("Sua aposta foi feita com sucesso!","");
        this.navCtrl.pop();
      }else
      this.utils.HidenLoad();
      
    });
  }
  DetailPage(){
    
    this.navCtrl.push("BetChampionPage");
  }

  slideChanged() {
    let currentIndex = this.slider.getActiveIndex();
    let slides_count = this.segments.nativeElement.childElementCount;

    this.page = currentIndex.toString();
    if(this.page >= slides_count)
      this.page = (slides_count-1).toString();

    console.log("slides_count",slides_count)
    console.log("this.page",this.page)
    this.centerScroll();
  }
 selectedTab(index) {
    this.slider.slideTo(index);
    
  }

  // Get size start to current
  sizeLeft(){
    let size = 0;
    for(let i = 0; i < this.page; i++){
      size+= this.segments.nativeElement.children[i].clientWidth;
    }
    return size;
  }
  centerScroll(){
    if(!this.segments || !this.segments.nativeElement)
      return;

    let sizeLeft = this.sizeLeft();
    let sizeCurrent = this.segments.nativeElement.children[this.page].clientWidth;
    let result = sizeLeft - (window.innerWidth / 2) + (sizeCurrent/2) ;

    result = (result > 0) ? result : 0;
    this.smoothScrollTo(result);
  }

  // Animate scroll
  smoothScrollTo(endX){
    let startTime = new Date().getTime();
    let startX = this.segments.nativeElement.scrollLeft;
    let distanceX = endX - startX;
    let duration = 400;

    let timer = setInterval(() => {
      var time = new Date().getTime() - startTime;
      var newX = this.easeInOutQuart(time, startX, distanceX, duration);
      if (time >= duration) {
        clearInterval(timer);
      }
      this.segments.nativeElement.scrollLeft = newX;
    }, 1000 / 60); // 60 fps
  }
   // Easing function
   easeInOutQuart(time, from, distance, duration) {
    if ((time /= duration / 2) < 1) return distance / 2 * time * time * time * time + from;
    return -distance / 2 * ((time -= 2) * time * time * time - 2) + from;
  }

}
