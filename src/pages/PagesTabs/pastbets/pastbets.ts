import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { global } from '../../../Controllers/test';
import { Utils } from '../../../Controllers/Utils/utils';
import { PostServiceProvider } from '../../../providers/post-service/post-service';
/**
 * Generated class for the PastbetsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pastbets',
  templateUrl: 'pastbets.html',
})
export class PastbetsPage {
  list:any=[];
  players: Array<{title: string,  background:string}>;
  constructor(public navCtrl: NavController, public navParams: NavParams,public utils:Utils,public postService:PostServiceProvider,) {
  }

  
    ionViewWillEnter() {
      /*this.User = global[0].user;
      this.players =[];
      this.players.push(
        {title:"Brasileiro",background:"brasila.jpg"},
        {title:"Copa do Brasil",background:"copadobrasl.png"},
        {title:"Libertadores",background:"libertadores.jpg"},
        {title:"Champions",background:"champions.png"},
        {title:"Champions",background:"champions.png"},
        {title:"Champions",background:"champions.png"},
        {title:"Champions",background:"champions.png"},
        {title:"Champions",background:"champions.png"},
        {title:"Champions",background:"champions.png"},
        {title:"Champions",background:"champions.png"},
        {title:"Champions",background:"champions.png"},
      )*/
      this.utils.Progress2("Carregando");
      this.list = [];
     
      let post = {
        "user_id":global[0].user.id,
        "status":3
      }
      this.postService.ListBetGames(post).then(async (_data:any)=>{
        
        _data.forEach(element => {
          this.list.push(element);
        });
        await this.utils.HidenLoad();
        (err)=>{console.log(err);
           this.utils.HidenLoad();}
      }).catch(err=> this.utils.HidenLoad());
  
    }
  
  Detail(game){
    this.navCtrl.push("BetPastDetailPage",{PastBet:game});
  }

}
