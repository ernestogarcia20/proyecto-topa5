import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { StorageClass }from '../../../Controllers/Storage/Storage';
import { PostServiceProvider } from '../../../providers/post-service/post-service';
import { Utils } from '../../../Controllers/Utils/utils';
import {global} from '../../../Controllers/test';
@IonicPage()
@Component({
  selector: 'page-more',
  templateUrl: 'more.html',
})
export class MorePage {

  constructor(public app : App,public Utils: Utils,public PostServiceProvider:PostServiceProvider,
    public StorageClass:StorageClass,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MorePage');
  }
  LogOut(){
    this.Utils.Progress("fazer logoff");
      let body ={
        "user_id": global[0].user.id,
    };
      this.PostServiceProvider.LogOut(body).then((data:any)=>{
        console.log(data)
        if(data){
          this.StorageClass.remove("User");
          this.StorageClass.remove("auto");
          this.StorageClass.remove("Token");
          this.app.getRootNav().setRoot("LoginPage");
        }else{
          this.Utils.HidenLoad()
        }
      }).catch(ex=>this.Utils.HidenLoad());

  }
  Password(){
    this.navCtrl.push("NewPasswordPage",{Token:global[0].data.token,isDasboard:true});
  }
  Profile(){
    this.navCtrl.push("ProfilePage");
  }

}
