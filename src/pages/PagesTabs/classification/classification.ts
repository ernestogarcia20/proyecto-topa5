import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ClassificationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-classification',
  templateUrl: 'classification.html',
})
export class ClassificationPage {
Title:string;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.Title = navParams.get('Title');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClassificationPage');
  }

}
