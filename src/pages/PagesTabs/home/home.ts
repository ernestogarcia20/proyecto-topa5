import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StorageClass }from '../../../Controllers/Storage/Storage';
import {sesion} from '../../../Controllers/sesion';
import { global,balance } from '../../../Controllers/test';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  DataUser:user;
  Balance:Balance;
  wallet;
  constructor(public Sesion:sesion,public StorageClass:StorageClass,public navCtrl: NavController) {
    
  }

  ionViewWillEnter(){
     /*this.Sesion.User().then((data)=>{
      this.DataUser = data;
    });*/

    
    this.Balance = balance[0];
    this.wallet = parseFloat(this.Balance.wallet).toFixed(2);
    this.DataUser = global[0].user;
}


  Tapped(type:string){
    if(type=="games"){
    this.navCtrl.push("LeaguesPage",{isGamen:true});
    }
    else if(type=="classification"){
    this.navCtrl.push("LeaguesPage",{isGamen:false});
    }
    else
    this.navCtrl.push("RankingPage");
  }
 

}
