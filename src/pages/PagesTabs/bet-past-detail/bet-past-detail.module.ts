import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BetPastDetailPage } from './bet-past-detail';

@NgModule({
  declarations: [
    BetPastDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BetPastDetailPage),
  ],
})
export class BetPastDetailPageModule {}
