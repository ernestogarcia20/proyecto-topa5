import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the BetPastDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bet-past-detail',
  templateUrl: 'bet-past-detail.html',
})
export class BetPastDetailPage {
DataGame;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
   this.DataGame= this.navParams.get('PastBet');
    console.log('ionViewDidLoad BetPastDetailPage');
  }

}
