import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { global,balance } from '../../../Controllers/test';
import { Utils } from '../../../Controllers/Utils/utils';
import { PostServiceProvider } from '../../../providers/post-service/post-service';
@Component({
  selector: 'page-mybet',
  templateUrl: 'mybet.html'
})
export class MyBetPage {
  User:user;
  player: Array<{title: string,  background:string}>;
  list:any=[];
  Balance:Balance;
  wallet;
  constructor(public navCtrl: NavController,public utils:Utils,public postService:PostServiceProvider,) {

  }
  ionViewWillEnter() {
    
    this.utils.Progress2("Carregando");
    this.list = [];
    this.User = global[0].user;
    this.Balance = balance[0];
    this.wallet = parseFloat(this.Balance.wallet).toFixed(2);
    let post = {
      "user_id":global[0].user.id,
      "status":1
    }
    this.postService.ListBetGames(post).then(async (_data:any)=>{
      
      _data.forEach(element => {
        this.list.push(element);
      });
      await this.utils.HidenLoad();
      (err)=>{console.log(err);
         this.utils.HidenLoad();}
    }).catch(err=> this.utils.HidenLoad());

  }
  Detail(){
    this.navCtrl.push("BetDetailPage");
  }

}
