import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import {global} from '../../../Controllers/test';
/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  public formgroup : FormGroup;
  username:string;
  email:string;
  country:string;
  idnetter:string;
  validation_messages:any={
    'username':[
      { type:'required', message:'O usuário é exigida'},
       ],
    'email':[
      { type:'required', message:'O correio é exigida'}
      ],
        
    'country':[
      { type:'required', message:'O país é exigida'}     
         ],
    'idnetter':[
              { type:'required', message:'O Id Neteller é exigida'}
            ],
  }
  constructor(public formBuilder:FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
    this.formgroup = this.formBuilder.group({
      username: new FormControl('', Validators.compose([
        Validators.required,
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        
      ])),
      country: new FormControl('', Validators.compose([
        Validators.required,
        
      ])),
      idnetter: new FormControl('', Validators.compose([
        Validators.required,
        
      ])),
    });
  }

  ionViewWillEnter() {
    console.log(global)
   this.email= global[0].user.email;
   this.username = global[0].user.username;
   
  }

}
