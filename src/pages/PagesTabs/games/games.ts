import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, } from 'ionic-angular';
import { PostServiceProvider } from '../../../providers/post-service/post-service';
import { Utils } from '../../../Controllers/Utils/utils';
import { timingSafeEqual } from 'crypto';

/**
 * Generated class for the GamesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-games',
  templateUrl: 'games.html',
})
export class GamesPage {
  @ViewChild('slider') slider: Slides;
  @ViewChild("segments") segments;
  page: any;
  players: Array<{title: string,  background:string}>;
  list:any=[];
  listPast:any=[];
  limit = 10;
  constructor(public utils:Utils,public postService:PostServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
    
  }
   ionViewDidLoad() {
    this.utils.Progress2("Carregando");
    this.list = [];
    let post = {
      "league":this.navParams.get('Id')
    }

    this.postService.Games(post).then(async (data:any)=>{
      data.forEach(element => {
        this.list.push(element);
      });
      console.log(this.list);
     //await this.utils.HidenLoad();
    }).catch((ex:any)=>{
     // this.utils.HidenLoad();
    });

    let postPast = {
      "league":this.navParams.get('Id')
    }

    this.listPast = [];
    this.postService.ListPastGames(postPast).then(async (data:any)=>{
      data.forEach(element => {
        this.listPast.push(element);
      });
      console.log(this.listPast);
     await this.utils.HidenLoad();

    }).catch((ex:any)=>{
      this.utils.HidenLoad();
    });
    
  }
  ionViewWillEnter() {
    this.slideChanged();
    
    
  }
  
  doInfinite(): Promise<any> {
    console.log('Begin async operation');

    return new Promise((resolve) => {
      this.limit += 5;
        console.log('Async operation has ended');
        resolve();
      
    })
  }

  DetailPage(games){
    this.navCtrl.push("BetPage",{Games:games,Liga:this.navParams.get('Id')});

  }

  slideChanged() {
    let currentIndex = this.slider.getActiveIndex();
    let slides_count = this.segments.nativeElement.childElementCount;

    this.page = currentIndex.toString();
    if(this.page >= slides_count)
      this.page = (slides_count-1).toString();

    console.log("slides_count",slides_count)
    console.log("this.page",this.page)
    this.centerScroll();
  }
 selectedTab(index) {
    this.slider.slideTo(index);
    
  }

  // Get size start to current
  sizeLeft(){
    let size = 0;
    for(let i = 0; i < this.page; i++){
      size+= this.segments.nativeElement.children[i].clientWidth;
    }
    return size;
  }
  centerScroll(){
    if(!this.segments || !this.segments.nativeElement)
      return;

    let sizeLeft = this.sizeLeft();
    let sizeCurrent = this.segments.nativeElement.children[this.page].clientWidth;
    let result = sizeLeft - (window.innerWidth / 2) + (sizeCurrent/2) ;

    result = (result > 0) ? result : 0;
    this.smoothScrollTo(result);
  }

  // Animate scroll
  smoothScrollTo(endX){
    let startTime = new Date().getTime();
    let startX = this.segments.nativeElement.scrollLeft;
    let distanceX = endX - startX;
    let duration = 400;

    let timer = setInterval(() => {
      var time = new Date().getTime() - startTime;
      var newX = this.easeInOutQuart(time, startX, distanceX, duration);
      if (time >= duration) {
        clearInterval(timer);
      }
      this.segments.nativeElement.scrollLeft = newX;
    }, 1000 / 60); // 60 fps
  }
   // Easing function
   easeInOutQuart(time, from, distance, duration) {
    if ((time /= duration / 2) < 1) return distance / 2 * time * time * time * time + from;
    return -distance / 2 * ((time -= 2) * time * time * time - 2) + from;
  }
}
