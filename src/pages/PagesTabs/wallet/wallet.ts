import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { global,balance } from '../../../Controllers/test';
/**
 * Generated class for the WalletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-wallet',
  templateUrl: 'wallet.html',
})
export class WalletPage {
User:user;
Balance:Balance;
  wallet;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter() {
    this.Balance = balance[0];
    this.wallet = parseFloat(this.Balance.wallet).toFixed(2);
    this.User = global[0].user;
    console.log('ionViewDidLoad WalletPage');
  }

}
