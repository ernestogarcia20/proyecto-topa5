import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Tabs, Tab } from 'ionic-angular';
import { PastbetsPage } from '../pastbets/pastbets';
import { MyBetPage } from '../mybet/mybet';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
enable:boolean = true;
tapped:boolean = true;
  @ViewChild('myTab') tabRef: Tabs;
  tab1Root = MyBetPage;
  tab2Root = PastbetsPage;
  tab3Root = HomePage;
  tab4Root = "WalletPage";
  tab5Root = "MorePage";

  constructor(public navCtrl: NavController,) {
    
  }
  
  ionViewWillEnter() {
    this.tabRef.select(2);
    console.log(this.tabRef.selectedIndex)
	}
  tabSelected(tab: Tab) {
    console.log(tab.index);
    if(tab.index==2)
     this.tapped=true
     else
     this.tapped=false
  }
  TappedTab(){
    this.tabRef.select(2);
    this.enable = false;
    this.tapped=true
  }
}
