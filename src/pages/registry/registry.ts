import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Keyboard } from 'ionic-angular';
import { Utils } from '../../Controllers/Utils/utils';
import { PostServiceProvider } from '../../providers/post-service/post-service';
import { Validators, FormBuilder, FormGroup, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
/**
 * Generated class for the RegistryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-registry',
  templateUrl: 'registry.html',
})
export class RegistryPage {
  private formgroup : FormGroup;
  username:string ="";
  email:string="";
  reemail:string="";
  password:string="";
  repassword:string="";
  validation_messages:any={
    'email':[
    { type:'required', message:'O correio é exigida'},
    { type:'pattern', message:'Ex: correio@exemplo.com'},
    { type:'validEmail', message:'Este e-mail foi registrado anteriormente'}
    ],
    'username':[
    { type:'required', message:'O usuário é exigida'},
    { type:'minlength', message: 'Deve conter mais de 5 caracteres' },
    { type:'validUser', message:'Um usuário com o nome de usuário selecionado já existe.'}
    ],
    'reemail':[
      { type:'required', message:'Confirme o email é exigida'},
      { type:'equalTo', message: 'A confirmação de email não corresponde' },
      ],
      'password':[
        { type:'required', message:'A senha é exigida'},
        { type:'minlength', message: 'A senha deve conter no mínimo 6 caracteres' },
        { type:'pattern', message: 'A senha deve conter uma letra Maiúscula, Minúscula, Numérica e um Caracater especial' },
        { type:'maxlength', message: 'A senha deve conter menos de 20 caracteres' },
        ],
        'repassword':[
          { type:'required', message:'Confirme senha é exigida'},
          { type:'equalTo', message: 'A confirmação da senha não corresponde' },
          ],
    }
  constructor(public formBuilder:FormBuilder,public utils:Utils,public postService:PostServiceProvider,
    public keyboard: Keyboard,public navCtrl: NavController, public navParams: NavParams) {
    this.formgroup = this.formBuilder.group({
      username: new FormControl('',[Validators.required, Validators.minLength(5)]),
      email: new FormControl('',Validators.compose([
        Validators.required,
        
        Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/),
        
      ])),
      reemail: new FormControl('',Validators.compose([
        Validators.required,
        this.equalto('email')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ -/:-@\[-`{-~]).{6,64}$/),
        Validators.minLength(6),
        Validators.maxLength(20),
        
      ])),
      repassword: new FormControl('', Validators.compose([
        Validators.required,
        this.equalto('password')
      ])),

    });
    //this.formgroup.untouched
    
  }
  registry(){
    
    let body ={
  "username": this.formgroup.value.username,
  "email": this.formgroup.value.email,
  "password": this.formgroup.value.password,
  };
   
    this.utils.Progress("criando conta");
    this.postService.registry(body).then((data:any)=>{
      var response = JSON.parse(data)
      if(response.success){
        this.utils.HidenLoad();
        this.Back();
        this.utils.PopUp("Conta Criada",
        "Para completar o cadastro enviamos um email para sua conta");
      }else{
        this.utils.HidenLoad();
        
        if(response.error.username){
          this.formgroup.controls.username.setErrors({
            "validUser":true
          });
          
        }
        else if(response.error.email){
          this.formgroup.controls.email.setErrors({
           "validEmail":true
        });
        }
        this.reemail = "";
        
      }
      
    }).catch((ex:any)=>{
      this.utils.HidenLoad();
      console.log(ex.error)
      if(ex.status == 3){
        this.utils.PopUp("Não há conexão com a internet","Tente novamente");
      }
    });;
   
    
     }
  
  keyboardCheck() {
    return !this.keyboard.isOpen();
   }
  ionViewDidLoad() {
    
    console.log('ionViewDidLoad RegistryPage');
  }
  Back(){
    this.navCtrl.pop();
  }
 
  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
    
    let input = control.value;
    
    let isValid=control.root.value[field_name]==input
    if(!isValid && input) 
    return { 'equalTo': {isValid} }
    else 
    return ;
    };
    }
}
