import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import {TabsPage} from '../PagesTabs/tabs/tabs';
import { PostServiceProvider } from '../../providers/post-service/post-service';
import { HttpErrorResponse } from '@angular/common/http';
import { Utils } from '../../Controllers/Utils/utils';
import { StorageClass }from '../../Controllers/Storage/Storage';
import { global,balance } from '../../Controllers/test';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
email:string = "edgarciav@outlook.com";
password:string= "Ernesto18.";
test:any={
  "success": true,
  "data": {
      "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjQsImV4cCI6MTU0ODQ0Mzk3OH0.uhoCZDYOUY3d-KAvqhojlm7Sxmys5o_JxEXRqa6v-hw"
  },
  "user": {
      "id": 4,
      "verified": 1,
      "active": 1,
      "username": "edgarcia",
      "email": "edgarciav@outlook.com",
      "exchange_fiat": "USD",
      "created": "2019-01-14T16:36:56+00:00",
      "modified": "2019-01-22T19:19:38+00:00",
      "photo": null,
      "language_id": 3,
      "2fa_status": null,
      "2fa_key": null,
      "2fa_active": 0
  }
};
  constructor(public app : App,public StorageClass:StorageClass,
    public utils:Utils,public postService:PostServiceProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter() {
    
  }

  Login(){
    this.utils.Progress("começando sessão");
    let body ={
      "email": this.email,
      "password": this.password
  };
  
    
this.postService.login(body).then((data:any)=>{
  var response = JSON.parse(data)
  console.log(response)
  if(response.success){
    this.StorageClass.set("Token",response.data.token)
    //this.StorageClass.set("auto",body)
    global.length = 0;
    global.push(response);
    let post = {
      "user_id":global[0].user.id
    }
    this.app.getRootNav().setRoot(TabsPage);
    this.postService.Balance(post).then((data:any)=>{
      if(data!= null){

      balance.length = 0;
      console.log(data)
      balance.push(data);
      
      }else{
        this.utils.HidenLoad();
      }
    }).catch(ex=>{this.utils.HidenLoad();});
    
   
  }else{
    alert("E-mail ou senha incorretos")
    this.utils.HidenLoad();
  }
  
},(err: HttpErrorResponse) => {
  this.utils.HidenLoad();
  if(err.status==401){
    alert("E-mail ou senha incorretos")
  }
  if (err.error instanceof Error) {
    console.log('Client-side error occured.');
  } else {
    if(err.status == 3){
      this.utils.PopUp("Não há conexão com a internet","Tente novamente");
    }
  }
  
}).catch((ex:any)=>{
  this.utils.HidenLoad();
  console.log(ex.error)
  if(ex.status == 3){
    this.utils.PopUp("Não há conexão com a internet","Tente novamente");
  }
})
  /*global.length = 0;
    global.push(this.test);
    this.app.getRootNav().setRoot((TabsPage));*/
  }
  RecoverPassword(){
    this.navCtrl.push('RecoverPasswordPage');
  }
  Registry(){
    this.navCtrl.push('RegistryPage');
  }
}
