import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, ViewController } from 'ionic-angular';
import { CatalogServiceProvider } from '../../providers/catalog-service/catalog-service';
import { Utils } from '../../Controllers/Utils/utils';
import { PostServiceProvider } from '../../providers/post-service/post-service';
/**
 * Generated class for the ConfirmaccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-confirmaccount',
  templateUrl: 'confirmaccount.html',
})
export class ConfirmaccountPage {
Token:string;
IsChangepassword:boolean;
  constructor(public app:App, private viewCtrl: ViewController,
    public postservice:PostServiceProvider,public utils:Utils,public navCtrl: NavController, public navParams: NavParams,public catalog:CatalogServiceProvider) {
    console.log(navParams.get('isChangepassword'))
    console.log(navParams.get('Token'))
    this.Token = navParams.get('Token');
    this.IsChangepassword = navParams.get('isChangepassword');
    this.verify();
  }
verify(){
  this.utils.Progress("Verificando");
  if(this.IsChangepassword){
    this.catalog.ValidPasswordToken(this.Token).then(
      (data:any) => { // Success
  
       if(data.success){
         
        this.utils.HidenLoad();
        this.navCtrl.push("NewPasswordPage",{Token:data.data[0].c.token,isDasboard:false}).then(() => {
          // first we find the index of the current view controller:
          const index = this.viewCtrl.index;
          // then we remove it from the navigation stack
          this.navCtrl.remove(index);
        });

        /*  this.Back();
          this.utils.PopUp("Conta Verificada","Agora você pode entrar na sua conta");*/
       }else{
        this.utils.HidenLoad();
       }
      },
      (error) =>{
        this.utils.HidenLoad();
        console.error(error);
        if(error.status == 3){
          this.utils.PopUp("Não há conexão com a internet","Tente novamente");
        }
      }
    ).catch((ex:any)=>{
      this.utils.HidenLoad();
      console.log(ex.error)
      if(ex.status == 3){
        this.utils.PopUp("Não há conexão com a internet","Tente novamente");
      }
    });;

  }else{
  this.catalog.verifyAccount(this.Token).then(
    (data:any) => { // Success

     if(data.success){
      this.utils.HidenLoad();
        this.Back();
        this.utils.PopUp("Conta Verificada","Agora você pode entrar na sua conta");
     }else{
      this.utils.PopUp("Ocorreu um problema ao verificar a conta","Tente novamente");
      this.utils.HidenLoad();
     }
    },
    (error) =>{
      this.utils.HidenLoad();
      console.error(error);
    }
  );
}
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmaccountPage');
  }
 Back(){
  this.navCtrl.pop();
  }
}
