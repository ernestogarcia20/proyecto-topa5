import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmaccountPage } from './confirmaccount';

@NgModule({
  declarations: [
    ConfirmaccountPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmaccountPage),
  ],
})
export class ConfirmaccountPageModule {}
