import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { PostServiceProvider } from '../../providers/post-service/post-service';
import { Utils } from '../../Controllers/Utils/utils';
/**
 * Generated class for the NewPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-new-password',
  templateUrl: 'new-password.html',
})
export class NewPasswordPage {
Token:string;
private formgroup : FormGroup;
  password:string="";
  repassword:string="";
  isDasboard:boolean=false;
validation_messages:any={
  'password':[
    { type:'required', message:'A senha é exigida'},
    { type:'minlength', message: 'A senha deve conter no mínimo 6 caracteres' },
    { type:'pattern', message: 'A senha deve conter uma letra Maiúscula, Minúscula, Numérica e um Caracater especial' },
    { type:'maxlength', message: 'A senha deve conter menos de 20 caracteres' },
    ],
    'repassword':[
      { type:'required', message:'Confirme senha é exigida'},
      { type:'equalTo', message: 'A confirmação da senha não corresponde' },
      ],
}
  constructor(public app:App,public utils:Utils,public postService:PostServiceProvider,public formBuilder:FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
   
    this.isDasboard=navParams.get('isDasboard')
    console.log(navParams.get('Token'))
    this.Token = navParams.get('Token');
    this.formgroup = this.formBuilder.group({
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[ -/:-@\[-`{-~]).{6,64}$/),
        Validators.minLength(6),
        Validators.maxLength(20),
        
      ])),
      repassword: new FormControl('', Validators.compose([
        Validators.required,
        this.equalto('password')
      ])),
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewPasswordPage');
  }
  changePassword(){
    this.utils.Progress("Validando");
    let body={
      "confirmpassword": this.formgroup.value.repassword,
      "password":this.formgroup.value.password,
      "token":this.Token 
    }

    this.postService.ChangePassword(body).then((data:any)=>{
      var response = JSON.parse(data);
      if(response.success){
        this.utils.PopUp("Senha Corretamente Modificada","");
        if(!this.isDasboard){
        //this.app.getRootNav().setRoot("LoginPage");
        this.navCtrl.pop()
        }
        else
        this.utils.HidenLoad();
      }else{
        this.utils.HidenLoad();
        this.utils.PopUp("Houve um erro","Tente novamente");
      }

    }).catch((ex:any)=>{
      this.utils.HidenLoad();
      console.log(ex.error)
      if(ex.status == 3){
        this.utils.PopUp("Não há conexão com a internet","Tente novamente");
      }
    });
  }
  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} => {
    
    let input = control.value;
    
    let isValid=control.root.value[field_name]==input
    if(!isValid && input) 
    return { 'equalTo': {isValid} }
    else 
    return ;
    };
    }

}
