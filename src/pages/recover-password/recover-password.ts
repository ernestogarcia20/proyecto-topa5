import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Utils } from '../../Controllers/Utils/utils';
import { PostServiceProvider } from '../../providers/post-service/post-service';

/**
 * Generated class for the RecoverPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recover-password',
  templateUrl: 'recover-password.html',
})
export class RecoverPasswordPage {
  private formgroup : FormGroup;
  validation_messages:any={
    'email':[
    { type:'required', message:'O correio é exigida'},
    { type:'pattern', message:'Ex: correio@exemplo.com'},
    
    ],
  }
  constructor(public app:App,public utils:Utils,public postService:PostServiceProvider,public formBuilder:FormBuilder,public navCtrl: NavController, public navParams: NavParams) {
    this.formgroup = this.formBuilder.group({
     
      email: new FormControl('',Validators.compose([
        Validators.required,
        Validators.pattern(/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/),
        
      ])) 
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecoverPasswordPage');
  }
  newpass(){
    this.utils.Progress("Validando");
    let body={
      "email":this.formgroup.value.email
    }
    this.postService.ForgotPassword(body).then((data:any)=>{
      var response = JSON.parse(data);
      this.utils.HidenLoad();
      if(response.success){
        
        this.Back();
        this.utils.PopUp("E-mail Enviado","Enviamos um email para sua conta para recuperar sua senha");
      }else{
        this.utils.PopUp("Correo no enviado","Tente novamente");
      }

    }).catch((ex:any)=>{
      this.utils.HidenLoad();
      console.log(ex.error)
      if(ex.status == 3){
        this.utils.PopUp("Não há conexão com a internet","Tente novamente");
      }
    });

  }
  Back(){
    this.app.getRootNav().pop();
  }

}
