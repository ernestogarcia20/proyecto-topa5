import { SecureStorage, SecureStorageObject } from '@ionic-native/secure-storage';
import { Injectable } from '@angular/core';
@Injectable()
export class StorageClass {
  
    constructor(private secureStorage: SecureStorage) { }
  
    public set(Name,value){
        return this.secureStorage.create('secure')
        .then((storage: SecureStorageObject) => {
           storage.set(`${ Name }`, value)
             .then(
               data => console.log(data),
               error => console.log(error)
           );
        }); 
      }
      public async get(Name){

        return this.secureStorage.create('secure')
      .then((storage: SecureStorageObject) => {
        storage.get(`${ Name }`)
        .then(
          data => console.log(data),
          error => console.log(error)
        );
      }); 
      }
      public  remove(Name){
        return this.secureStorage.create('secure')
      .then((storage: SecureStorageObject) => {
        storage.remove(`${ Name }`)
        .then(
          data => console.log(data),
          error => console.log(error)
        );
      }); 
      }
      public clear() {
        return this.secureStorage.create('secure')
      .then((storage: SecureStorageObject) => {
        storage.clear()
        .then(
          data => console.log(data),
          error => console.log(error)
        );
      }); 
      }
  }