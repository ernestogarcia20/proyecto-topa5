import { LoadingController, Loading } from 'ionic-angular';
import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
@Injectable()
export class Utils {
    loading: Loading;
    constructor(public alertCtrl: AlertController,public loadingCtrl: LoadingController) { }
  
    Progress(text) {
        this.loading = this.loadingCtrl.create({
        content:text,// "começando sessão",
        dismissOnPageChange:true,
      });
      this.loading.present();
    }
    Progress2(text) {
      this.loading = this.loadingCtrl.create({
      content:text,// "começando sessão",
      dismissOnPageChange:false,
    });
    this.loading.present();
  }
    HidenLoad() {
        this.loading.dismiss();
    }

    PopUpValidate(message) {
        let alert = this.alertCtrl.create({
          title: 'IMPORTANTE',
          cssClass: 'custom-message',
          subTitle: message,
          buttons: [{text:"Ok"}]
        });
        alert.present();
      }
      PopUp(title,message) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle:message,
          cssClass: 'custom-message',
          buttons: [{text:"Ok"}]
        });
        alert.present();
      }

  }