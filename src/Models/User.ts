interface user{
    id:number;
    username:string;
    email:string;
    language_id:number;
    active:number;
    exchange_fiat:string;
    verified:number;
    fa_active:number;
    created:string;
    modified:string;
    photo:string;
}